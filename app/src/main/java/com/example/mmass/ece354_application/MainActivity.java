package com.example.mmass.ece354_application;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button calculate = (Button) findViewById(R.id.calculate_button);

        final TextView answer = (TextView) findViewById(R.id.answer_textView);
        final TextView from = (TextView) findViewById(R.id.from_textView);
        final TextView to = (TextView) findViewById(R.id.to_textView);

        final Spinner disciplines = (Spinner) findViewById(R.id.disciplines_spinner);
            ArrayAdapter<CharSequence> disciplines_adapter = ArrayAdapter.createFromResource(this, R.array.disciplines_list, android.R.layout.simple_spinner_item);
        disciplines_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        disciplines.setAdapter(disciplines_adapter);

        final Spinner equations = (Spinner) findViewById(R.id.equations_spinner);
            final ArrayAdapter<CharSequence> favorites_adapter = ArrayAdapter.createFromResource(this, R.array.favorites_list, android.R.layout.simple_spinner_item);
                favorites_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            final ArrayAdapter<CharSequence> empty_adapter = ArrayAdapter.createFromResource(this, R.array.empty_list, android.R.layout.simple_spinner_item);
                empty_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            final ArrayAdapter<CharSequence> civil_adapter = ArrayAdapter.createFromResource(this, R.array.civil_list, android.R.layout.simple_spinner_item);
                civil_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            final ArrayAdapter<CharSequence> computer_adapter = ArrayAdapter.createFromResource(this, R.array.computer_list, android.R.layout.simple_spinner_item);
                computer_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            final ArrayAdapter<CharSequence> electrical_adapter = ArrayAdapter.createFromResource(this, R.array.electrical_list, android.R.layout.simple_spinner_item);
                electrical_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            final ArrayAdapter<CharSequence> mechanical_adapter = ArrayAdapter.createFromResource(this, R.array.mechanical_list, android.R.layout.simple_spinner_item);
                mechanical_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            final ArrayAdapter<CharSequence> other_adapter = ArrayAdapter.createFromResource(this, R.array.other_list, android.R.layout.simple_spinner_item);
                other_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        equations.setAdapter(empty_adapter);

        final Spinner base_from_select = (Spinner) findViewById(R.id.from_spinner);
            final ArrayAdapter<CharSequence> base_from_adapter = ArrayAdapter.createFromResource(this, R.array.base_conversion, android.R.layout.simple_spinner_item);
                base_from_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        base_from_select.setAdapter(base_from_adapter);

        final Spinner base_to_select = (Spinner) findViewById(R.id.to_spinner);
            final ArrayAdapter<CharSequence> base_to_adapter = ArrayAdapter.createFromResource(this, R.array.base_conversion, android.R.layout.simple_spinner_item);
                base_to_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        base_to_select.setAdapter(base_to_adapter);


        disciplines.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parentView, View view, int position, long id){
                if(position == 0){
                    equations.setVisibility(View.INVISIBLE);
                }
                else if(position == 1){
                    equations.setVisibility(View.VISIBLE);
                    //equations.setAdapter(FAVORITES);
                }
                else if(position == 2){
                    equations.setVisibility(View.VISIBLE);
                    Discipline civil = new Discipline(position);
                    equations.setAdapter(civil_adapter);
                }
                else if(position == 3){
                    equations.setVisibility(View.VISIBLE);
                    Discipline computer = new Discipline(position);
                    equations.setAdapter(computer_adapter);
                }
                else if(position == 4){
                    equations.setVisibility(View.VISIBLE);
                    Discipline electical = new Discipline(position);
                    equations.setAdapter(electrical_adapter);
                }
                else if(position == 5){
                    equations.setVisibility(View.VISIBLE);
                    Discipline mechanical = new Discipline(position);
                    equations.setAdapter(mechanical_adapter);
                }
                else if(position == 6){
                    equations.setVisibility(View.VISIBLE);
                    Discipline other = new Discipline(position);
                    equations.setAdapter(other_adapter);
                }
                else{
                    equations.setAdapter(empty_adapter);
                    equations.setVisibility(View.INVISIBLE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView){
                equations.setVisibility(View.INVISIBLE);
            }
        });

        equations.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(disciplines.getSelectedItemPosition() == 3){
                    if (position == 1){
                       base_from_select.setVisibility(View.VISIBLE);
                       from.setVisibility(View.VISIBLE);
                       base_to_select.setVisibility(View.VISIBLE);
                       to.setVisibility(View.VISIBLE);
                       calculate.setVisibility(View.VISIBLE);
                       answer.setVisibility(View.VISIBLE);
                    }
                }
                else{
                    base_from_select.setVisibility(View.INVISIBLE);
                    from.setVisibility(View.INVISIBLE);
                    base_to_select.setVisibility(View.INVISIBLE);
                    to.setVisibility(View.INVISIBLE);
                    calculate.setVisibility(View.INVISIBLE);
                    answer.setVisibility(View.INVISIBLE);
                    answer.setText(" ");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                base_from_select.setVisibility(View.INVISIBLE);
                from.setVisibility(View.INVISIBLE);
                base_to_select.setVisibility(View.INVISIBLE);
                to.setVisibility(View.INVISIBLE);
                calculate.setVisibility(View.INVISIBLE);
                answer.setVisibility(View.INVISIBLE);
                answer.setText(" ");
            }
        });
    }
}
