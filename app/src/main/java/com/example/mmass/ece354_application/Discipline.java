package com.example.mmass.ece354_application;

public class Discipline {
    private static String[] equations;

    public static String[] getEquations() { return equations; }

    public Discipline(int discipline){
        switch(discipline){
            case 0:{
                break;
            }
            //case for Favorites
            case 1:{
                //equations[] = new String[xyz];
                //code to append size as favorites are added or removed
            }
            //case for Civil
            case 2:{
                equations = new String[]{"Civil 1", "Civil 2"};
            }
            //case for Computer
            case 3:{
                equations = new String[]{"Computer 1, Computer 2"};
            }
            //case for Electrical
            case 4:{
                //equations[] = {};
            }
            //case for Mechanical
            case 5:{
                //equations[] = {};
            }
            //case for Other
            case 6:{
                //equations[] = {};
            }
        }
    }
}
